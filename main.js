const WebSocket = require('ws')
const express = require('express')
const path = require('path')

const PORT = process.env.PORT || 3000;

console.log("# Listening on PORT:", PORT);

const app = express()

app.use('/', express.static('static'));

//app.get('/', function (req, res) {
//    console.log("# GET:", req.url)
//    res.sendFile(path.join(__dirname+'/static/index.html'));
//})

const server = app.listen(PORT)
const wss = new WebSocket.Server({ server: server})

class Baguette {
    constructor(){
        console.log("Baguette.constructor")
        this.table = {}
        this.ondeadws = null
    }

    printTable(){
        console.log("Baguette.printTable()")
        const print = Object.keys(this.table).forEach((k) => {
            let key = (k == "[object Object]") ? "ws" : k ;
            let val = (this.table[k] instanceof WebSocket) ? "ws" : this.table[k] ;
            console.log("Baguette.table:", key, "->", val)
        })
    }

    registerName(name, ws){
        console.log("Baguette.registerName(", name,")")

        if(this.table[name] || this.table[ws]) {
            console.log("Baguette.registerName:", "duplicate name/ws")
            return false
        }

        this.table[name] = ws
        this.table[ws] = name

        ws.onclose = (e) => this.removeWebSocket(ws)

        console.log("Baguette.registerName:", "sucess")

        this.printTable()
        return true
    }

    removeName(name){
        console.log("Baguette.removeName(", name, ")")
        const ws = this.table[name]
        if(ws){
            console.log("Baguette.removeName:","found name")
            delete this.table[name]
            this.removeWebSocket(ws)
        }
        this.printTable()
    }

    removeWebSocket(ws){
        console.log("Baguette.removeWebSocket(", (ws ? "ws" : "??"), ")")
        const name = this.table[ws]
        if(name){
            delete this.table[ws]
            console.log("Baguette.removeWebSocket:", "deleted ws")

            if(this.ondeadws){
                console.log("Baguette.removeWebSocket:", "call ondeadws")
                this.ondeadws(ws)
            }

            this.removeName(name)
        }
        this.printTable()
    }
}

class Signaler{
    constructor(){
        console.log("Signaler.constructor")
        this.table = {}
        this.baguette = new Baguette()
        this.baguette.ondeadws = (ws) => this.deadws(ws)
    }

    printTable(){
        console.log("Signaler.printTable()")
        const print = Object.keys(this.table).forEach((k) => {
            let key = (k instanceof WebSocket) ? "ws" : k ;
            let val = this.table[k] ;
            if(val instanceof Array){
                console.log("#", key, "->", val.map((v) => (v instanceof WebSocket) ? "ws" : v))
            }else{
                console.log("#", key, "->", (val instanceof WebSocket) ? "ws" : val)
            }
        })
    }

    deadws(ws){
        console.log("Signaler.deadws(", (ws ? "ws" : "??"), ")")
        const pcid = this.table[ws]
        if(pcid){
            console.log("Signaler.deadws:", "found table[ws] pcid")
            delete this.table[ws]

            delete this.table[pcid]
            console.log("Signaler.deadws:", "delete table[pcid]")

            const wsList = this.table[pcid]
            if(wsList){
                console.log("Signaler.deadws:", "loop through wsList with length:", wsList.length)
                for(let i=0; i<wsList.length; i++){
                    console.log("#", "delete table[ws]")
                    delete this.table[wsList[i]]
                }
            }
        }

        this.printTable()
    }

    signal(ws, signal){
        console.log("Signaler.signal(", (ws ? "ws" : "??"), (signal ? "signal" : "??"), ")")
        const {pcid, name} = signal

        console.log("# [pcid, name]", [pcid, name])

        let wsList = this.table[pcid]

        if(wsList){
            console.log("Signaler.signal:", "wsList found")
            console.log("Signaler.signal:", "return forward")
            return this.forward(ws, wsList, signal)
        }

        if(name){
            console.log("Signaler.signal:", "using baguette to find name")
            const wsBaguette = this.baguette.table[name]

            if(wsBaguette){
                console.log("Signaler.signal:", "name found")

                wsList = [ws, wsBaguette]

                // check if client socket is coming from a server
                if(!this.baguette.table[ws]){
                    ws.onclose = (e) => this.deadws(ws)
                }

                console.log("Signaler.signal:", "create wsList")

                this.table[pcid] = wsList
                console.log("Signaler.signal:", "set table["+pcid+"] to wsList")

                console.log("Signaler.signal:", "iterate over wsList")
                for(let i=0; i<wsList.length; i++){
                    this.table[wsList[i]] = pcid
                }

                console.log("Signaler.signal:", "return forward")
                return this.forward(ws, wsList, signal)
            }
        }

        const response = {type: "no-name", pcid}
        if(name){
            response['name'] = name
        }
        this.sendmsg(ws, response)

        this.printTable()
        return false
    }

    forward(ws, wsList, signal){
        console.log("Signaler.forward(", (ws ? "ws" : "??"), (wsList ? "wsList" : "??"), (signal ? "signal" : "??"), ")")
        const idx = wsList.indexOf(ws)
        if(idx == -1 && wsList.length > 1) return false

        console.log("Signaler.forward:", "idx:", idx)

        for(let i=0; i<wsList.length; i++){
            if(i == idx) continue
            this.sendmsg(wsList[i], signal)
        }

        return (wsList.length > 1)
        this.printTable()
    }

    sendmsg(ws, msg){
        console.log("Signaler.sendmsg(", (ws ? "ws" : "??"), (msg ? "msg" : "??"), ")")
        ws.send(JSON.stringify(msg))
    }

    recvmsg(e){
        console.log("Signaler.recvmsg(e)")
        const ws = e.target
        const msg = this._parse(e.data)

        if(!msg) return false

        console.log("# sender:", msg.sender)

        // register-me event
        if(msg.type == "register-me"){
            console.log("# type is register-me")
            let status = msg.name &&
                         msg.name.length &&
                         this.baguette.registerName(msg.name, ws)

            console.log("Signaler.recvmsg:","registered:", status)
            this.sendmsg(ws, {type: "register-me", name: msg.name, status})
        }

        // signal event
        else if(msg.type == "signal"){
            console.log("Signaler.recvmsg:", "type is signal")
            const b = this.signal(ws, msg)
            console.log("Signaler.recvmsg:", "signal() ->",b)
        }

        else{
            console.log("Signaler.recvmsg:","cant parse type:", msg.type)
        }
    }

    _parse(str){
        try{
            console.log("Signaler._parse(str)")
            const msg = JSON.parse(str)
            return msg
        }catch(err){
            console.log("Error parsing message:", e.data)
            console.log(err)
            return null
        }
    }
}

const signaler = new Signaler()

let msgCount = 1
function _recvmsg(e){
    console.log("######## ----- num: "+(msgCount++)+" ---------------------------- ########")
    signaler.recvmsg(e)
}

wss.on('connection', ws => {
    console.log("got connection")
    ws.onmessage = _recvmsg
})
