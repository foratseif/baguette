'use-strict'

class Server extends PeerConnectionManager{

    static CONNECTED = 2;
    static CONNECTING = 1;
    static NOT_CONNECTED = 0;

    constructor(){
        super({impolite: false})

        this.status = Server.NOT_CONNECTED
        this.registeredName = ""

        this.onreq = null
        this.onstatuschange = null
    }

    registerOnDNS(){
        if(this.name == this.registeredName &&
            this.status == Server.CONNECTED){
            return
        }

        // create websocket to connect right now
        this.status = Server.CONNECTING
        if(this.onstatuschange){
            this.onstatuschange(this.status)
        }
        this.websocket().then((ws) => {
            this.send({
                type: "register-me",
                name: this.name
            })
        })
        .catch((e) => {
            console.log("ERROR:", e)
            this.status = Server.NOT_CONNECTED
            if(this.onstatuschange){
                this.onstatuschange(this.status)
            }
        })
    }

    handleMsg(msg){
        if (msg.type == "register-me"){
            this.status = msg.status ?
                Server.CONNECTED : Server.NOT_CONNECTED

            if(this.onstatuschange){
                this.onstatuschange(this.status)
            }

            this.registeredName = msg.name
        }
        else super.handleMsg(msg)
    }

    _pcConnected(dc, pcid, pc){

    }

    _pcMessage(msg, pc, pcid){
        try{
            const req = new Request(msg)
            pc.send(this.onreq(req))
        }catch{
            pc.send("503")
        }
    }

    _wsClose(ws, e){
        this.ws = null

        this.status = Server.NOT_CONNECTED
        if(this.onstatuschange){
            this.onstatuschange(this.status)
        }

        console.log("ws closed", e)
    }
}
