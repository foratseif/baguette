// use strict mode for improved performance
'use-strict'

// class PeerConnection
//  Handles communcation with the DNS server,
//  sending sdp offers/answers, and exchanging ICE
//  candidates
class PeerConnection {

    constructor({impolite, onsignal, onconnected, onmessage}){

        this.onsignal = onsignal
        this.impolite = impolite ? true : false
        this.connected = false

        // create RTCPeerConnection
        this.pc = new RTCPeerConnection({
            //iceServers: [
            //    {
            //        urls: [
            //            "stun:stun.l.google.com:19302",
            //            "stun:stun1.l.google.com:19302",
            //        ],
            //    },
            //],
            iceServers: [
                {
                    urls: "stun:openrelay.metered.ca:80"
                },
                {
                    urls: "turn:openrelay.metered.ca:80",
                    username: "openrelayproject",
                    credential: "openrelayproject"
                },
                {
                    urls: "turn:openrelay.metered.ca:443",
                    username: "openrelayproject",
                    credential: "openrelayproject"
                },
                {
                    urls: "turn:openrelay.metered.ca:443?transport=tcp",
                    username: "openrelayproject",
                    credential: "openrelayproject"
                },
            ],
        });

        // set RTCPeerConnection prenegotiated data channel
        this.dataChannel = this.pc.createDataChannel("MyAppChannel", {
            negotiated: true,
            id: 69,
        });

        this.dataChannel.onopen = () => {
            for(let i=0; i<10; i++){
                let x = "!";
                for(let j=0; j<i; j++){
                    x=x+"!"
                }
                console.log("DATACHANNEL OPEN", x)
            }
            this.connected = true
            if(onconnected) onconnected(this)
        }

        this.dataChannel.onmessage = (m) => {
            if(onmessage){
                const msg = this._parseJSON(m.data)
                onmessage(msg)
            }
        }

        this.makingOffer = false;

        this.pc.onnegotiationneeded = async () => {
            try {
                this.makingOffer = true;
                await this.pc.setLocalDescription();
                this.onsignal({ description: this.pc.localDescription });
            } catch(err) {
                console.error(err);
            } finally {
                this.makingOffer = false;
            }
        }

        this.pc.onicecandidate = ({candidate}) => {
            console.log("sending ICE CANDIDATE")
            if( !candidate ) return
            if( this.connected ) return
            this.onsignal({candidate});
        }

        this.pc.icecandidateerror = (e) => {
            console.log("IceCandidateError:", e)
        }

        this.ignoreOffer = false;
    }

    async handleSignal({ description, candidate }){
        try {
            if (description) {
                const offerCollision =
                    (description.type == "offer") &&
                    (this.makingOffer || this.pc.signalingState != "stable");

                this.ignoreOffer = this.impolite && offerCollision;
                if (this.ignoreOffer) {
                    return;
                }

                await this.pc.setRemoteDescription(description);
                if (description.type == "offer") {
                    await this.pc.setLocalDescription();
                    this.onsignal({ description: this.pc.localDescription })
                }
            } else if (candidate) {
                try {
                    await this.pc.addIceCandidate(candidate);
                } catch(err) {
                    if (!this.ignoreOffer) {
                        throw err;
                    }
                }
            }
        } catch(err) {
            console.error(err);
        }
    }

    send(msg){
        this.dataChannel.send(JSON.stringify(msg))
    }

    close(){
        this.dataChannel.close()
        this.pc.close()
    }

    _parseJSON(str){
        try{
            const msg = JSON.parse(str)
            return msg
        }catch(err){
            console.log("Error parsing message:", str)
            console.log(err)
            return "420"
        }
    }

}

