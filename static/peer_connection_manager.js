'use-strict'

class PeerConnectionManager {

    static WEBSOCKET_URL = location.origin.replace(/^http/, 'ws')

    constructor({impolite}){
        this.impolite = impolite
        this.connections = {}
        this.ws = null
        this._wsPromise = null
    }

    createPeerConnection(vars){
        console.log("CREATING A FUCKING PEER CONNECTION!!!")
        const pcid = vars.pcid ? vars.pcid : Math.ceil(Math.random()*100000)

        const pc = new PeerConnection({
            impolite: this.impolite,
            onsignal: (sig) => {
                let msg = {type: "signal", pcid, sig}
                if(vars.name){
                    msg['name'] = vars.name
                }
                this.send(msg)
            },
            onconnected: () => {
                this._pcConnected(pc, pcid)
            },
            onmessage: (m) => this._pcMessage(m, pc, pcid),
        })

        this.connections[pcid] = pc
        console.log(this.connections)
        return [pcid, pc]
    }

    _pcConnected(pc, pcid){ }
    _pcMessage(msg, pc, pcid){ }

    connectToName(name){
        const tuple = this.createPeerConnection({name})
        return tuple // [pcid, pc]
    }

    async send(msg){
        console.log("have to send:", msg)
        const ws = await this.websocket()
        msg["sender"] = this.impolite ? "CLIENT" : "SERVER"
        ws.send(JSON.stringify(msg))
    }

    _parseJSON(str){
        try{
            const msg = JSON.parse(str)
            return msg
        }catch(err){
            console.log("Error parsing message:", e.data)
            console.log(err)
            return null
        }
    }

    handleMsg(msg){
        console.log("# RECIEVED msg: ", msg)
        if(msg.type == "signal"){
            const pc = this.connections[msg.pcid] ?
                        this.connections[msg.pcid] :
                        this.createPeerConnection({pcid: msg.pcid})[1] ;
            pc.handleSignal(msg.sig)
        }
        else{
            console.log("CANT HANDLE MSG:", msg)
        }
    }

    _wsMessage(ws, e){
        const msg = this._parseJSON(e.data)
        if(!msg) return
        this.handleMsg(msg)
    }

    _wsClose(ws, e){
        console.log("_wsClose")
        if(this.ws == ws){
            this.ws = null
        }
    }

    _wsOpen(ws, e){
        console.log("_wsOpen")
        if(this.ws && this.ws != ws){
            this.ws.close()
        }

        this.ws = ws
    }

    _wsError(ws, e){
        console.log("_wsError")
        this.ws = null
    }

    websocket(){
        if(this.ws){
            console.log("resuing WEBSOCKET")
            return Promise.resolve(this.ws)
        }
        else if(this._wsPromise){
            console.log("returning WEBSOCKET PROMISE")
            return this._wsPromise
        }
        else{
            console.log("CREATING WEBSOCKET")
            this._wsPromise = new Promise((res, rej) => {
                let ws = new WebSocket(PeerConnectionManager.WEBSOCKET_URL);

                ws.onmessage = (e) => this._wsMessage(ws, e)

                ws.onclose = (e) => this._wsClose(ws, e)

                ws.onopen = e => {
                    this._wsPromise = null
                    this._wsOpen(ws, e)
                    res(ws)
                }

                ws.onerror = e => {
                    this._wsError(ws, e)
                    rej(e)
                }
            })
            return this._wsPromise
        }
    }
}
