'use-strict'

class Client extends PeerConnectionManager{

    constructor(){
        super({impolite: true})
        this.requests = {}
    }

    _removeConnection(pcid){
        delete this.connections[pcid]
        if(Object.keys(this.connections) == 0){
            this.ws.close()
            this.ws = null
        }
    }

    _pcConnected(pc, pcid){
        this._removeConnection(pcid)

        if(this.requests[pcid]){
            const {req} = this.requests[pcid]
            pc.send(req)
        }
    }

    request(req){
        if(!req.bonjour || !req.bonjour.length){
            return Promise.reject("bonjour sucks")
        }

        const [pcid, pc] = this.connectToName(req.bonjour[0])

        let res, rej;
        const promise = new Promise((s, j) => {
            res = s;
            rej = j;
        })

        this.requests[pcid] = {promise, res, rej, req}
        return promise
    }

    handleMsg(msg){
        if(msg.type == "no-name"){
            console.log("NO NAME BRO:", msg)

            if(this.requests[msg.pcid]){
                const {rej} = this.requests[msg.pcid]
                delete this.requests[msg.pcid]
                rej("no-name")
            }

            const pc = this.connections[msg.pcid]
            if(pc){
                pc.close()
                this._removeConnection(msg.pcid)
            }
        }
        else super.handleMsg(msg)
    }

    _pcMessage(msg, pc, pcid){
        console.log("RECIEVED", msg)
        if(this.requests[pcid]){
            const {res} = this.requests[pcid]
            delete this.requests[pcid]
            res(msg)
        }
        //console.log("RECIEVED:", msg)
        //console.log("RECIEVED:", msg)
        //console.log("RECIEVED:", msg)
        //console.log("RECIEVED:", msg)
        //console.log("RECIEVED:", msg)
    }

    //_pcError(err, pcid, pc){
    //    this._removeConnection(pcid)
    //}

}
